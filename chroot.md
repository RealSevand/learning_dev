Заставить работать ls и cp
см Видео выше.
Вывод консоли в ответ:
```
$ mkdir lesson3
$ mkdir lesson3/bin
$ mkdir lesson3/lib
$ mkdir lesson3/lib64
$ cp /bin/bash lesson3/bin/
$ cp /bin/ls lesson3/bin/
$ cp /bin/cp lesson3/bin/
$ ldd lesson3/bin/bash
        linux-vdso.so.1 (0x00007ffc9ffe4000)
        libtinfo.so.6 => /lib64/libtinfo.so.6 (0x00007f833eac3000)
        libdl.so.2 => /lib64/libdl.so.2 (0x00007f833e8bf000)
        libc.so.6 => /lib64/libc.so.6 (0x00007f833e4fc000)
        /lib64/ld-linux-x86-64.so.2 (0x00007f833f00e000)
$ cp /lib64/libtinfo.so.6 lesson3/lib64/
$ cp /lib64/libdl.so.2 lesson3/lib64/
$ cp /lib64/libc.so.6 lesson3/lib64/
$ cp /lib64/ld-linux-x86-64.so.2 lesson3/lib64/
$ ldd lesson3/bin/ls
        linux-vdso.so.1 (0x00007fff614dc000)
        libselinux.so.1 => /lib64/libselinux.so.1 (0x00007f9931f65000)
        libcap.so.2 => /lib64/libcap.so.2 (0x00007f9931d60000)
        libc.so.6 => /lib64/libc.so.6 (0x00007f993199d000)
        libpcre2-8.so.0 => /lib64/libpcre2-8.so.0 (0x00007f9931719000)
        libdl.so.2 => /lib64/libdl.so.2 (0x00007f9931515000)
        /lib64/ld-linux-x86-64.so.2 (0x00007f99323b2000)
        libpthread.so.0 => /lib64/libpthread.so.0 (0x00007f99312f5000)
$ cp /lib64/libselinux.so.1 lesson3/lib64/
$ cp /lib64/libcap.so.2 lesson3/lib64/
$ cp /lib64/libc.so.6 lesson3/lib64/
$ cp /lib64/libpcre2-8.so.0 lesson3/lib64/
$ cp /lib64/libdl.so.2 lesson3/lib64/
$ cp /lib64/ld-linux-x86-64.so.2 lesson3/lib64/
$ cp /lib64/libpthread.so.0 lesson3/lib64/
$ ldd lesson3/bin/cp
        linux-vdso.so.1 (0x00007ffd94d11000)
        libselinux.so.1 => /lib64/libselinux.so.1 (0x00007f898a027000)
        libacl.so.1 => /lib64/libacl.so.1 (0x00007f8989e1e000)
        libattr.so.1 => /lib64/libattr.so.1 (0x00007f8989c18000)
        libc.so.6 => /lib64/libc.so.6 (0x00007f8989855000)
        libpcre2-8.so.0 => /lib64/libpcre2-8.so.0 (0x00007f89895d1000)
        libdl.so.2 => /lib64/libdl.so.2 (0x00007f89893cd000)
        /lib64/ld-linux-x86-64.so.2 (0x00007f898a475000)
        libpthread.so.0 => /lib64/libpthread.so.0 (0x00007f89891ad000)
$ cp /lib64/libselinux.so.1 lesson3/lib64/
$ cp /lib64/libacl.so.1 lesson3/lib64/
$ cp /lib64/libattr.so.1 lesson3/lib64/
$ cp /lib64/libc.so.6 lesson3/lib64/
$ cp /lib64/libpcre2-8.so.0  lesson3/lib64/
$ cp /lib64/libdl.so.2 lesson3/lib64/
$ cp /lib64/ld-linux-x86-64.so.2 lesson3/lib64/
$ cp /lib64/libpthread.so.0 lesson3/lib64/
$ sudo chroot /home/Admin/lesson3
[sudo] password for Admin:
bash-4.4# ls
bin  lib  lib64
bash-4.4# cp
cp: missing file operand
Try 'cp --help' for more information.
bash-4.4# exit
exit
```
